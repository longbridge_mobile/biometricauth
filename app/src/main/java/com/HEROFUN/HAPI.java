package com.HEROFUN;

import android.app.Activity;
import android.os.Handler;

public class HAPI {

 	//*********************************************************************************************************
	//*********************************************************************************************************
	//*********************************************************************************************************
    //----------------------------The below defines error codes---------------------------------------------//.
    public static final int ERROR_NONE = 0xFFFFFF0;
	private static final int ERROR_ARGUMENTS = 0xFFFFFF1;
    public static final int ERROR_LOW_QUALITY = 0xFFFFFF4;
	private static final int ERROR_NEG_FIND = 0xFFFFFF8;
	private static final int ERROR_NONE_CAPIMAGE = 0xFFFF21;
	private static final int ERROR_NOT_CALIBRATED = 0xFFFF22;
	private static final int ERROR_NONE_DEVICE = 0xFFFF24;
	private static final int ERROR_DO_CANCELED= 0xFFFF26;

   //-------------------The below defines message to be sent to Activity----------------------------------//.
    private static final int MSG_PUT_FINGER = 1;
	private static final int MSG_FINGER_CAPTURED = 4;
    public static final int MSG_SHOW_TEXT = 27;
    public static final int MSG_SHOW_VERIFICATION_FAILED = 28;
    public static final int MSG_SHOW_RETRY = 29;

   //-----------------The below defines object variables for CALLBACK of Activity-------------------------//.
    private Handler m_fHandler = null;

    //------------------------The below defines variables for error code ----------------------------------//
	private int m_errCode;
	private int m_nCaptureTime, m_nFeatureTime, m_nMatchTime;
    
    //--------------------The below defines object variables for LAPI Library------------------------------//.
    private LAPI m_hLIB;
    public int m_hDev= 0;
    private byte[] m_image = new byte[LAPI.WIDTH*LAPI.HEIGHT];
    private byte[] qr_minutiae_left = new byte[LAPI.FPINFO_SIZE];
    private byte[] qr_minutiae_right = new byte[LAPI.FPINFO_SIZE];
    private byte[] minutiae_right = new byte[LAPI.FPINFO_SIZE];
    private byte[] minutiae_left = new byte[LAPI.FPINFO_SIZE];
	private byte[] itemplateToMatch = new byte[LAPI.FPINFO_SIZE];
    private byte[] leftItemplateToMatched = new byte[LAPI.FPINFO_SIZE];
    private byte[] rightItemplateToMatched = new byte[LAPI.FPINFO_SIZE];
	private int DefFingerTheshold = LAPI.DEF_FINGER_SCORE;
	private int DefEnrollTheshold = LAPI.DEF_QUALITY_SCORE;
	private boolean m_bCancel = false;
	
	//*********************************************************************************************************
	//-----------------------------------------------------------------------------------------------------//
    public HAPI(Activity c, Handler cHandler) {
    	if (c==null) return;
    	m_fHandler = cHandler;
    	m_hLIB = new LAPI(c);
 	}
	//-----------------------------------------------------------------------------------------------------//
    public int GetErrorCode(){
    	return m_errCode;
    }

	//-----------------------------------------------------------------------------------------------------//
    public void DoCancel(){
    	m_bCancel = true;
    }
	//-----------------------------------------------------------------------------------------------------//
    public int GetProcessTime(int icase){
    	switch (icase) {
    	case 0: return m_nCaptureTime;
    	case 1: return m_nFeatureTime;
    	case 2: return m_nMatchTime;
    	}
    	return 0;
    }
	//-----------------------------------------------------------------------------------------------------//
	private void SendMessage(int message, int arg1, int arg2, Object obj) {
    	m_fHandler.obtainMessage(message, arg1, arg2, obj).sendToTarget();
    }

	public byte[] EnrollLeft (String regId, boolean option){
		int k, result, itry, qr, count;
		m_errCode = ERROR_NONE;
		m_bCancel = false;
		if (m_hDev==0) { m_errCode = ERROR_NONE_DEVICE; return null;}
		if (regId==null || regId.isEmpty()) { m_errCode = ERROR_ARGUMENTS; return null;}
    
		count = 0;
		itry = 0;
   		qr = 0;
		for ( k = 0; k < LAPI.FPINFO_SIZE; k ++ ) qr_minutiae_left[k] = 0;

		SendMessage(MSG_PUT_FINGER,0,0,"");

		   //Capture image
		   int ret_enroll_left = 0;
		   while (ret_enroll_left<DefFingerTheshold) {
				if (m_bCancel) {
					m_errCode = ERROR_DO_CANCELED;
				   return null;
				}
				ret_enroll_left = m_hLIB.GetImage(m_hDev,m_image);
				if (ret_enroll_left == LAPI.NOTCALIBRATED) {
					m_errCode = ERROR_NOT_CALIBRATED;
					return null;
				}
				if (ret_enroll_left == LAPI.FALSE) {
					m_errCode = ERROR_NONE_CAPIMAGE;
					return null;
				}
				ret_enroll_left = m_hLIB.IsPressFinger(m_hDev,m_image);
			}
			SendMessage(MSG_FINGER_CAPTURED,LAPI.WIDTH,LAPI.HEIGHT,m_image);

			//Create Template
			for ( k = 0; k < LAPI.FPINFO_SIZE; k ++ ) minutiae_left[k] = 0;

			if (option)
				result = m_hLIB.CreateANSITemplate(m_hDev,m_image,minutiae_left);
			else
				result = m_hLIB.CreateISOTemplate(m_hDev,m_image,minutiae_left);

			if (qr < result) {
				qr = result;
				for ( k = 0; k < LAPI.FPINFO_SIZE; k ++ ) qr_minutiae_left[k] = minutiae_left[k];
			}
			else if (qr>0 && qr==result) {
				count ++;
				if (count==3) return null;
			}
			else if (qr>0) {
				return null;
			}
			SendMessage(MSG_PUT_FINGER,itry,result,"");
	
		if (qr < DefEnrollTheshold ) {
			m_errCode = ERROR_LOW_QUALITY; 
			return null;
		}
		
		//Register Template as appointed Id
		return qr_minutiae_left;
	}

	public byte[] EnrollRight (String regId, boolean option){
		int k, result, itry, qr, count;
		m_errCode = ERROR_NONE;
		m_bCancel = false;
		if (m_hDev==0) { m_errCode = ERROR_NONE_DEVICE; return null;}
		if (regId==null || regId.isEmpty()) { m_errCode = ERROR_ARGUMENTS; return null;}

		count = 0;
		itry = 0;
   		qr = 0;
		for ( k = 0; k < LAPI.FPINFO_SIZE; k ++ ) qr_minutiae_right[k] = 0;

		SendMessage(MSG_PUT_FINGER,0,0,"");

		   //Capture image
		   int ret_enroll_right = 0;
		   while (ret_enroll_right<DefFingerTheshold) {
				if (m_bCancel) {
					m_errCode = ERROR_DO_CANCELED;
				   return null;
				}
				ret_enroll_right = m_hLIB.GetImage(m_hDev,m_image);
				if (ret_enroll_right == LAPI.NOTCALIBRATED) {
					m_errCode = ERROR_NOT_CALIBRATED;
					return null;
				}
				if (ret_enroll_right == LAPI.FALSE) {
					m_errCode = ERROR_NONE_CAPIMAGE;
					return null;
				}
				ret_enroll_right = m_hLIB.IsPressFinger(m_hDev,m_image);
			}
			SendMessage(MSG_FINGER_CAPTURED,LAPI.WIDTH,LAPI.HEIGHT,m_image);

			//Create Template
			for ( k = 0; k < LAPI.FPINFO_SIZE; k ++ ) minutiae_right[k] = 0;

			if (option)
				result = m_hLIB.CreateANSITemplate(m_hDev,m_image,minutiae_right);
			else
				result = m_hLIB.CreateISOTemplate(m_hDev,m_image,minutiae_right);

			if (qr < result) {
				qr = result;
				for ( k = 0; k < LAPI.FPINFO_SIZE; k ++ ) qr_minutiae_right[k] = minutiae_right[k];
			}
			else if (qr>0 && qr==result) {
				count ++;
				if (count==3) return null;
			}
			else if (qr>0) {
				return null;
			}
			SendMessage(MSG_PUT_FINGER,itry,result,"");

		if (qr < DefEnrollTheshold ) {
			m_errCode = ERROR_LOW_QUALITY;
			return null;
		}

		//Register Template as appointed Id
		return qr_minutiae_right;
	}
    //*********************************************************************************************************
	// Purpose   : Verify fingerprint through 1:1 Matching against to DATABSE 
	// Function  : Verify
	// Arguments : 
	//			(In) : String veriId : record tag to be verified
	//			(In) : boolean option : 1 - ANSI format, 0 - ISO format
	// Return    : boolean   
	//*********************************************************************************************************

	public boolean Verify (boolean option, byte[] leftFingerprint , byte[] rightFingerprint){
		int startTime ;
		m_nCaptureTime = 0;
		m_nFeatureTime = 0;
		m_nMatchTime = 0;

		m_errCode = ERROR_NONE;
		m_bCancel = false;

		if (m_hDev==0) { m_errCode = ERROR_NONE_DEVICE; return false;}
		leftItemplateToMatched = leftFingerprint;
		rightItemplateToMatched = rightFingerprint;
		if ((leftItemplateToMatched == null) && (rightItemplateToMatched == null)) {
			m_errCode = ERROR_NEG_FIND;
			return false;
		}

		SendMessage(MSG_PUT_FINGER,0,0,"");
		SendMessage(MSG_FINGER_CAPTURED,LAPI.WIDTH,LAPI.HEIGHT,null);
		int ret_verify = 0;

		//Capture Image
		while (ret_verify<DefFingerTheshold) {
			if (m_bCancel) {
				m_errCode = ERROR_DO_CANCELED;
				return false;
			}
			startTime = (int) System.currentTimeMillis();
			ret_verify = m_hLIB.GetImage(m_hDev,m_image);
			if (ret_verify == LAPI.NOTCALIBRATED) {
				m_errCode = ERROR_NOT_CALIBRATED;
				return false;
			}
			if (ret_verify == LAPI.FALSE) {
				m_errCode = ERROR_NONE_CAPIMAGE;
				return false;
			}
			m_nCaptureTime = (int) System.currentTimeMillis();
			m_nCaptureTime = m_nCaptureTime - startTime;
			ret_verify = m_hLIB.IsPressFinger(m_hDev,m_image);
		}
		SendMessage(MSG_FINGER_CAPTURED,LAPI.WIDTH,LAPI.HEIGHT,m_image);

		//Create Template
		int result, resultLeft, resultRight;
		for ( int k = 0; k < LAPI.FPINFO_SIZE; k ++ ) itemplateToMatch[k] = 0;
		startTime = (int) System.currentTimeMillis();

		if (option)
			result = m_hLIB.CreateANSITemplate(m_hDev,m_image,itemplateToMatch);
		else
			result = m_hLIB.CreateISOTemplate(m_hDev,m_image,itemplateToMatch);

		m_nFeatureTime = (int) System.currentTimeMillis();
		m_nFeatureTime = m_nFeatureTime - startTime;
		if (result==0) {
			m_errCode = ERROR_LOW_QUALITY;
			return false;
		}

		//1:1 Matching
		startTime = (int) System.currentTimeMillis();
		m_nMatchTime = (int) System.currentTimeMillis();
		m_nMatchTime = m_nMatchTime - startTime;
        resultLeft = m_hLIB.CompareTemplates(m_hDev,itemplateToMatch,leftItemplateToMatched);
        resultRight = m_hLIB.CompareTemplates(m_hDev, itemplateToMatch, rightItemplateToMatched);
		int defMatchTheshold = LAPI.DEF_MATCH_SCORE;
		return (resultRight >= defMatchTheshold) || (resultLeft >= defMatchTheshold);
    }
}
