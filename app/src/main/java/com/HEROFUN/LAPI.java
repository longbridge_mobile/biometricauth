package com.HEROFUN;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

import com.HEROFUN.HostUsb;

import android.app.Activity;
import android.util.Log;

public class LAPI {
    //****************************************************************************************************
	static 
	{
		try{
			System.loadLibrary("biofp_e_lapi");
		}
		catch(UnsatisfiedLinkError e) {
			Log.e("LAPI","biofp_e_lapi",e);
		}
	}
	//****************************************************************************************************
	static final File PWFILE_BH701 = new File("/sys/class/power_supply/usb/device/CONTROL_GPIO114");
	static final File PWFILE_BH502 = new File("/sys/class/power_supply/usb/device/CONTROL_GPIO83");
	//****************************************************************************************************
    public static final int MSG_BULK_TRANS_OUT = 0x13;
	//****************************************************************************************************
	public static final int WIDTH  = 256;
	public static final int HEIGHT  = 360;
    //****************************************************************************************************
	public static final int FPINFO_STD_MAX_SIZE = 1024;
    public static final int DEF_FINGER_SCORE = 45;
    public static final int DEF_QUALITY_SCORE = 30;
    public static final int DEF_MATCH_SCORE = 45;
	public static final int FPINFO_SIZE = FPINFO_STD_MAX_SIZE;
    //****************************************************************************************************
    public static final int FALSE = 0;
    public static final int NOTCALIBRATED = -2;
    //****************************************************************************************************
    public static final int SPI_MODE = 2;
    public static final int VERSION2 = 2;
    public static final int TABLET_DEFAULT = 0;
    public static final int TABLET_BH701 = 1;
    public static final int TABLET_BH502 = 2;
    public static final int commMode = SPI_MODE;
    public static final int versionNo = VERSION2;
    public static final int tabletNo = TABLET_DEFAULT;
    //****************************************************************************************************
    private static Activity m_content = null;
    //****************************************************************************************************
	public LAPI(Activity a) {
		m_content = a;
	}
	//****************************************************************************************************
	protected void POWER_ON(int tablet)
	{
		FileReader inCmd; 
		try {
			if (tablet == TABLET_BH701) inCmd = new FileReader(PWFILE_BH701);
			else if (tablet == TABLET_BH502) inCmd = new FileReader(PWFILE_BH502);
			else return;
	      	 inCmd.read();
	      	 inCmd.close();
	       }
	    catch (Exception e) {}

		try {
			Thread.sleep(30);
		} catch (InterruptedException e) {}
	}
    //****************************************************************************************************
	protected void POWER_OFF(int tablet)
	{
		FileWriter closefr;
        try {
			if (tablet == TABLET_BH701) closefr = new FileWriter(PWFILE_BH701);
			else if (tablet == TABLET_BH502) closefr = new FileWriter(PWFILE_BH502);
			else return;
            closefr.write("1"); 
            closefr.close();
        }
        catch (Exception e) {}

        try {
			Thread.sleep(30);
		} catch (InterruptedException e) {}
	}
	//------------------------------------------------------------------------------------------------//
	// Purpose   : This function initializes the Fingerprint Recognition SDK Library and 
	//				connects Fingerprint Collection Module.
 	// Function  : OpenDevice
	// Arguments : void
	// Return    : int  
	//			     If successful, return handle of device, else 0. 	
	//------------------------------------------------------------------------------------------------//
	private native int OpenDevice(int commMode, int versionNo);
	public int OpenDeviceEx()
	{
		int ret = 0;
		POWER_ON(tabletNo);
		ret = OpenDevice(commMode, versionNo);
		if (ret == 0) POWER_OFF(tabletNo);
		return ret;
	}
	//------------------------------------------------------------------------------------------------//
	// Purpose   : This function finalizes the Fingerprint Recognition SDK Library and 
	//				disconnects Fingerprint Collection Module.
 	// Function  : CloseDevice
	// Arguments : 
	//      (In) : int device : handle returned from function "OpenDevice()"
	// Return    : int
	//			      If successful, return 1, else 0 	
	//------------------------------------------------------------------------------------------------//
	private  native int CloseDevice(int device);
	public int CloseDeviceEx(int device)
	{
		int ret;
		ret = CloseDevice(device);
		POWER_OFF(tabletNo);
		return ret;
	}
	//------------------------------------------------------------------------------------------------//
	// Purpose   : This function returns image captured from Fingerprint Collection Module.
	// Function  : GetImage
	// Arguments : 
	//      (In) : int device : handle returned from function "OpenDevice()"
	//  (In/Out) : byte[] image : image captured from this device
	// Return    : int
	//			      If successful, return 1,
	//				  if not calibrated(TCS1/2), return -2,		
	//						else, return  0 	
	//------------------------------------------------------------------------------------------------//
	public native int GetImage(int device, byte[] image);
	//------------------------------------------------------------------------------------------------//
	// Purpose   : This function checks whether finger is on sensor of this device or not.
	// Function  : IsPressFinger
	// Arguments : 
	//      (In) : int device : handle returned from function "OpenDevice()"
	//		(In) : byte[] image : image returned from function "GetImage()"
	// Return    : int 
	//				   return percent value indicating that finger is placed on sensor(0~100). 	
	//------------------------------------------------------------------------------------------------//
	public native int IsPressFinger(int device,byte[] image);
	//------------------------------------------------------------------------------------------------//
	// Purpose   : This function creates the ANSI standard template from the uncompressed raw image. 
	// Function  : CreateANSITemplate
	// Arguments : 
	//      (In) : int device : handle returned from function "OpenDevice()"
	//		(In) : byte[] image : image returned from function "GetImage()"
	//	(In/Out) : byte[] itemplate : ANSI standard template created from image.
	// Return    : int : 
	//				   If this function successes, return none-zero, else 0. 	
	//------------------------------------------------------------------------------------------------//
	public native int CreateANSITemplate(int device,byte[] image, byte[] itemplate);
	//------------------------------------------------------------------------------------------------//
	// Purpose   : This function creates the ISO standard template from the uncompressed raw image. 
	// Function  : CreateISOTemplate
	// Arguments : void
	//      (In) : int device : handle returned from function "OpenDevice()"
	//		(In) : byte[] image : image returned from function "GetImage()"
	//  (In/Out) : byte[] itemplate : ISO standard template created from image.
	// Return    : int : 
	//				   If this function successes, return none-zero, else 0. 	
	//------------------------------------------------------------------------------------------------//
	public native int CreateISOTemplate(int device,byte[] image,  byte[] itemplate);
	//------------------------------------------------------------------------------------------------//
	// Purpose   : This function matches two templates and returns similar match score.
	//             This function is for 1:1 Matching and only used in fingerprint verification. 
	// Function  : CompareTemplates
	// Arguments : 
	//      	(In) : int device : handle returned from function "OpenDevice()"
	//			(In) : byte[] itemplateToMatch : template to match : 
	//                 This template must be used as that is created by function "CreateANSITemplate()"  
	//                 or function "CreateISOTemplate()".
	//			(In) : byte[] itemplateToMatched : template to be matched
	//                 This template must be used as that is created by function "CreateANSITemplate()"  
	//                 or function "CreateISOTemplate()".
	// Return    : int 
	//					return similar match score(0~100) of two fingerprint templates.
	//------------------------------------------------------------------------------------------------//
	public native int CompareTemplates(int device,byte[] itemplateToMatch, byte[] itemplateToMatched);
	//****************************************************************************************************
}
