package com.parlegrandpa.bioauth.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.parlegrandpa.bioauth.R;
import com.parlegrandpa.bioauth.util.ManageSQLDatabase;

public class ConfirmationActivity extends AppCompatActivity {

    private ImageView img_left_thumb, img_right_thumb;
    private byte[] leftByteArray, rightByteArray;
    private int accountId;
    private ManageSQLDatabase manageSQLDatabase;
    private String TAG = "ConfirmationActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmation);

        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        manageSQLDatabase = new ManageSQLDatabase(this);

        leftByteArray = getIntent().getByteArrayExtra("leftByteArray");
        rightByteArray = getIntent().getByteArrayExtra("rightByteArray");
        accountId = getIntent().getIntExtra("accountId", 0);

        init();
    }

    private void init()
    {
        img_left_thumb = findViewById(R.id.img_left_thumb);
        ShowFingerBitmap( 0);
        img_right_thumb = findViewById(R.id.img_right_thumb);
        ShowFingerBitmap(1);
        Button btn_submit = findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

            boolean checkAccount = manageSQLDatabase.recordExist(Integer.toString(accountId));
            if(checkAccount)
            {
                Log.d(TAG, "Record already exist...");
            }
            else
            {
                if(manageSQLDatabase.insertRecord(Integer.toString(accountId), leftByteArray, rightByteArray))
                {
                    Log.d(TAG, "Record saved");
                    startActivity(new Intent(ConfirmationActivity.this, EnrollActivity.class));
                }
                else
                {
                    Log.d(TAG, "Error while saving...");
                }
            }
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void ShowFingerBitmap(int thumb) {
        if(thumb == 1) {
            img_right_thumb.setImageResource(R.drawable.finger_exist);
        }
        else
        {
            img_left_thumb.setImageResource(R.drawable.finger_exist);
        }
    }
}

