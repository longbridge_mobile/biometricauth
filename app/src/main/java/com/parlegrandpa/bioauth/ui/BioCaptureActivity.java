package com.parlegrandpa.bioauth.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.HEROFUN.HAPI;
import com.HEROFUN.LAPI;
import com.parlegrandpa.bioauth.R;

public class BioCaptureActivity extends AppCompatActivity {

    private Button btn_left_thumb;
    private Button btn_right_thumb;
    private ImageView img_left_thumb, img_right_thumb;

    private volatile boolean bContinue = false;
    private boolean option = true;
    private HAPI mHAPI = null;
    private LAPI mLAPI = null;
    private int m_Device = 0;
    private int accountId;
    private byte[] leftByteArray, rightByteArray;

    public static final int MESSAGE_FINGER_LEFT_ERROR = 100;
    public static final int MESSAGE_FINGER_RIGHT_ERROR = 101;
    public static final int MESSAGE_FINGER_LEFT_CAPTURE = 102;
    public static final int MESSAGE_FINGER_RIGHT_CAPTURE = 103;

    public static final int MESSAGE_SHOW_TEXT = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bio_capture);

        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        accountId = getIntent().getIntExtra("accountId", 0);

        mLAPI = new LAPI(this);
        mHAPI = new HAPI(this, m_fpsdkHandle);

        openDevice();

        init();
        EnableButtons(true, false);
    }

    public void openDevice()
    {
        new openDeviceAsynk().execute();
    }

    public void closeDevice()
    {
        new closeDeviceAsynk().execute();
    }

    protected void OPEN_DEVICE() {
        String msg;
        m_Device = mLAPI.OpenDeviceEx();
        if (m_Device==0) msg = "Can't open device !";
        else {
            msg = "OpenDevice() = OK";
        }
        mHAPI.m_hDev = m_Device;
        m_fEvent.sendMessage(m_fEvent.obtainMessage(MESSAGE_SHOW_TEXT, 0, 0,msg));
    }

    protected void CLOSE_DEVICE() {
        mHAPI.DoCancel();
        if (m_Device != 0) {
            mLAPI.CloseDeviceEx(m_Device);
        }
        m_Device = 0;
        mHAPI.m_hDev = m_Device;
    }

    private void init()
    {
        img_left_thumb = findViewById(R.id.img_left_thumb);
        img_right_thumb = findViewById(R.id.img_right_thumb);

        Button btn_proceed = findViewById(R.id.btn_proceed);
        btn_proceed.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if((leftByteArray != null) && (rightByteArray != null)) {
                    Intent intent = new Intent(BioCaptureActivity.this, ConfirmationActivity.class);
                    intent.putExtra("leftByteArray", leftByteArray);
                    intent.putExtra("rightByteArray", rightByteArray);
                    intent.putExtra("accountId", accountId);
                    startActivity(intent);
                }
            }
        });

        btn_left_thumb = findViewById(R.id.btn_left_thumb);
        btn_left_thumb.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (bContinue) {
                    mHAPI.DoCancel();
                    bContinue = false;
                    return;
                }
                bContinue = true;

                new enrollLeftFingerAsynk().execute();
            }
        });

        btn_right_thumb = findViewById(R.id.btn_right_thumb);
        btn_right_thumb.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (bContinue) {
                    mHAPI.DoCancel();
                    bContinue = false;
                    return;
                }
                bContinue = true;

                new enrollRightFingerAsynk().execute();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    @Override
    protected void onDestroy() {
        if (m_Device != 0) closeDevice();
        super.onDestroy();
    }


    protected void FINGER_ENROLL_LEFT() {

        String regid = Integer.toString(accountId);
        if ((regid==null) || regid.isEmpty()) {
            EnableButtons(true, false);
            bContinue = false;
            return;
        }

        byte[] retLeft = mHAPI.EnrollLeft(regid, option);
        if((retLeft != null))
        {
            leftByteArray = retLeft;
            handlerEvent.sendMessage(handlerEvent.obtainMessage(MESSAGE_FINGER_LEFT_CAPTURE, LAPI.WIDTH, LAPI.HEIGHT, leftByteArray));
        }
        else
        {
            handlerEvent.sendMessage(handlerEvent.obtainMessage(MESSAGE_FINGER_LEFT_ERROR));
        }

        bContinue = false;
    }

    protected void FINGER_ENROLL_RIGHT() {

        String regid = Integer.toString(accountId);
        if ((regid==null) || regid.isEmpty()) {
            EnableButtons(true, false);
            bContinue = false;
            return;
        }

        byte[] retRight = mHAPI.EnrollRight(regid, option);
        if((retRight != null))
        {
            rightByteArray = retRight;
            handlerEvent.sendMessage(handlerEvent.obtainMessage(MESSAGE_FINGER_RIGHT_CAPTURE, LAPI.WIDTH, LAPI.HEIGHT, rightByteArray));
        }
        else
        {
            handlerEvent.sendMessage(handlerEvent.obtainMessage(MESSAGE_FINGER_RIGHT_ERROR));
        }

        bContinue = false;
    }

    public void EnableButtons(Boolean leftThumbButton, Boolean rightThumbButton)
    {
        btn_left_thumb.setEnabled(leftThumbButton);
        btn_right_thumb.setEnabled(rightThumbButton);
    }

    private void ShowFingerBitmap(int thumb) {
        if(thumb == 1) {
            img_right_thumb.setImageResource(R.drawable.finger_exist);
        }
        else
        {
            img_left_thumb.setImageResource(R.drawable.finger_exist);
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class openDeviceAsynk extends AsyncTask<Void, Void, Void>{

        protected Void doInBackground(Void... voids) {
            OPEN_DEVICE ();
            return null;
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class closeDeviceAsynk extends AsyncTask<Void, Void, Void>{

        protected Void doInBackground(Void... voids) {
            CLOSE_DEVICE ();
            return null;
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class enrollLeftFingerAsynk extends AsyncTask<Void, Void, Void>{

        protected Void doInBackground(Void... voids) {
            FINGER_ENROLL_LEFT ();
            return null;
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class enrollRightFingerAsynk extends AsyncTask<Void, Void, Void>{

        protected Void doInBackground(Void... voids) {
            FINGER_ENROLL_RIGHT ();
            return null;
        }
    }

    @SuppressLint("HandlerLeak")
    private final Handler handlerEvent = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_FINGER_LEFT_ERROR:
                    EnableButtons(true, false);
                    break;
                case MESSAGE_FINGER_RIGHT_ERROR:
                    EnableButtons(false, true);
                    break;
                case MESSAGE_FINGER_RIGHT_CAPTURE:
                    ShowFingerBitmap(1);
                    EnableButtons(false, false);
                    break;
                case MESSAGE_FINGER_LEFT_CAPTURE:
                    ShowFingerBitmap(0);
                    EnableButtons(false, true);
                    break;
            }
        }
    };

    @SuppressLint("HandlerLeak")
    private final Handler m_fpsdkHandle = new Handler() {
 		@Override
        public void handleMessage(Message msg) {
		}
        };

    private final Handler m_fEvent = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_SHOW_TEXT:
                    String TAG = "BioCaptureActivity";
                    Log.d(TAG, (String)msg.obj);
                    break;
            }
        }
    };
}
