package com.parlegrandpa.bioauth.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.SparseArray;
import android.widget.Button;

import com.HEROFUN.HAPI;
import com.HEROFUN.LAPI;
import com.lusfold.spinnerloading.SpinnerLoading;
import com.parlegrandpa.bioauth.R;
import com.parlegrandpa.bioauth.util.ManageSQLDatabase;

import java.util.HashMap;
import java.util.Map;

public class CheckFingerprintActivity extends AppCompatActivity {

    private volatile boolean bContinue = false;
    private HAPI m_HAPI = null;
    private LAPI m_LAPI = null;
    private int m_Device = 0;
    private String accountId;
    private ManageSQLDatabase manageSQLDatabase;

    private String TAG = "CheckFingerprintActivity";

    public static final int MESSAGE_SHOW_TEXT = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_fingerprint);

        configureSpinner();

        m_LAPI = new LAPI(this);
        m_HAPI = new HAPI(this, m_fpsdkHandle);
        manageSQLDatabase = new ManageSQLDatabase(this);

        accountId = getIntent().getStringExtra("accountId");

        openDevice();

        init();
    }

    @Override
    protected void onPostResume() {
        openDevice();
        super.onPostResume();
    }

    public void configureSpinner()
    {
        SpinnerLoading view = findViewById(R.id.spinner_loading);
        view.setPaintMode(1);
        view.setCircleRadius(10);
        view.setItemCount(8);
    }

    public void init()
    {
        if (bContinue) {
            m_HAPI.DoCancel();
            bContinue = false;
            return;
        }
        bContinue = true;

//        new verfyFingerAsynk().execute();
    }

    public void openDevice()
    {
        new openDeviceAsynk().execute();
    }

    public void closeDevice()
    {
        new closeDeviceAsynk().execute();
    }

    protected void OPEN_DEVICE() {
        String msg;
        m_Device = m_LAPI.OpenDeviceEx();
        if (m_Device==0) msg = "Can't open device !";
        else {
            msg = "OpenDevice() = OK";
            if (bContinue) {
                m_HAPI.DoCancel();
                bContinue = false;
                return;
            }
            bContinue = true;

            new verfyFingerAsynk().execute(fetchRecords(accountId));
        }
        m_HAPI.m_hDev = m_Device;
        m_fEvent.sendMessage(m_fEvent.obtainMessage(MESSAGE_SHOW_TEXT, 0, 0,msg));
    }

    protected void CLOSE_DEVICE() {
        m_HAPI.DoCancel();
        if (m_Device != 0) {
            m_LAPI.CloseDeviceEx(m_Device);
        }
        m_Device = 0;
        m_HAPI.m_hDev = m_Device;
    }

    private SparseArray<byte[]> fetchRecords(String account)
    {
        boolean checkAccount = manageSQLDatabase.recordExist(account);
        if(checkAccount)
        {
            return manageSQLDatabase.fetchRecord(accountId);
        }

        return null;
    }

    protected void FINGER_VERIFY(SparseArray<byte[]> customerData) {
        int retry;
        String msg = "";
        String regid = accountId;

        if ((regid==null) || regid.isEmpty()) {
            bContinue = false;
            return;
        }

        if(customerData != null)
        {
            byte[] leftByteArray = customerData.get(0);
            byte[] rightByteArray = customerData.get(1);

            for (retry = 0; retry < 10; retry ++  ) {
                boolean option = true;
                boolean ret = m_HAPI.Verify(option, leftByteArray, rightByteArray);
                if(ret)
                {
                    msg = String.format("Verify OK (ID=%s) : Time(Capture=%dms,Create=%dms,Match=%dms)",
                            regid,m_HAPI.GetProcessTime(0),m_HAPI.GetProcessTime(1),m_HAPI.GetProcessTime(2));

                    Intent intent = new Intent(CheckFingerprintActivity.this, CustomerInfoActivity.class);
                    intent.putExtra("accountId", accountId);
                    startActivity(intent);
                    break;
                }
                else
                {
                    int errCode = m_HAPI.GetErrorCode();
                    if (errCode != HAPI.ERROR_NONE && errCode != HAPI.ERROR_LOW_QUALITY) {
                        msg = "Unable to verify finger ";
                        break;
                    }
                    if(errCode == 268435440)
                    {
                        msg = "Unable to verify finger ";
                        m_fpsdkHandle.obtainMessage(HAPI.MSG_SHOW_VERIFICATION_FAILED, 0, 0, msg).sendToTarget();
                    }
                }
                SLEEP(300);
            }

            if (retry == 10) {
                msg = "Verify : False : Exceed retry limit";
                m_fpsdkHandle.obtainMessage(HAPI.MSG_SHOW_RETRY, 0, 0, msg).sendToTarget();
            }
        }
        else
        {
            msg = "Record not found ";
        }

        bContinue = false;
        m_fpsdkHandle.obtainMessage(HAPI.MSG_SHOW_TEXT, 0, 0, msg).sendToTarget();
    }

    protected void SLEEP (int waittime)
    {
        int startTime, passTime = 0;
        startTime = (int)System.currentTimeMillis();
        while (passTime < waittime) {
            passTime = (int)System.currentTimeMillis();
            passTime = passTime - startTime;
        }
    }

    @Override
    protected void onDestroy() {
        if (m_Device != 0) closeDevice();
        super.onDestroy();
    }

    @SuppressLint("StaticFieldLeak")
    private class openDeviceAsynk extends AsyncTask<Void, Void, Void> {

        protected Void doInBackground(Void... voids) {
            OPEN_DEVICE ();
            return null;
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class closeDeviceAsynk extends AsyncTask<Void, Void, Void>{

        protected Void doInBackground(Void... voids) {
            CLOSE_DEVICE ();
            return null;
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class verfyFingerAsynk extends AsyncTask<SparseArray<byte[]>, Void, Void>{

        protected Void doInBackground(SparseArray<byte[]>... params) {
            FINGER_VERIFY (params[0]);
            return null;
        }
    }

        private final Handler m_fEvent = new Handler() {
    @Override
    public void handleMessage(Message msg) {
        switch (msg.what) {
            case MESSAGE_SHOW_TEXT:
                Log.d(TAG, (String)msg.obj);
                break;
        }
        }
    };

    @SuppressLint("HandlerLeak")
    private final Handler m_fpsdkHandle = new Handler() {
        @Override
        public void handleMessage(Message msg) {
        switch (msg.what) {
            case 0xff:
                break;
            case HAPI.MSG_SHOW_TEXT:
                Log.d(TAG, (String)msg.obj);
                break;
            case HAPI.MSG_SHOW_VERIFICATION_FAILED:
                Log.d(TAG, (String)msg.obj);
                break;
            case HAPI.MSG_SHOW_RETRY:
                Log.d(TAG, (String)msg.obj);
                break;
        }
        }
    };
}
