package com.parlegrandpa.bioauth.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.parlegrandpa.bioauth.R;
import com.parlegrandpa.bioauth.util.ManageSQLDatabase;
import com.parlegrandpa.bioauth.util.Validation;

public class EnrollActivity extends AppCompatActivity {

    EditText txt_account_number;
    private ManageSQLDatabase manageSQLDatabase;
    private String TAG = "EnrollActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enroll);

        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        init();
    }

    private void init()
    {
        manageSQLDatabase = new ManageSQLDatabase(this);
        txt_account_number = findViewById(R.id.txt_account_number);
        txt_account_number.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                Validation.isPhoneNumber(txt_account_number, false);
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after){}
            public void onTextChanged(CharSequence s, int start, int before, int count){}
        });

        Button btn_biometricCapture = findViewById(R.id.btn_biometricCapture);
        btn_biometricCapture.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(checkValidation())
                {
                    boolean checkAccount = manageSQLDatabase.recordExist(txt_account_number.getText().toString());
                    if(checkAccount)
                    {
                        Log.d(TAG, "Record already exist");
                    }
                    else {
                        Intent intent = new Intent(EnrollActivity.this, BioCaptureActivity.class);
                        intent.putExtra("accountId", Integer.parseInt(txt_account_number.getText().toString()));
                        startActivity(intent);
                    }
                }
                else
                {
                    Log.d(TAG, "Form contains error");
                }
            }
        });
    }

    private boolean checkValidation() {
        boolean ret = true;

        if (!Validation.isPhoneNumber(txt_account_number, false)
                || (txt_account_number.getText().length() == 0)
                || (txt_account_number.getText().length() > 10) )
        {
            ret = false;
        }
        return ret;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
