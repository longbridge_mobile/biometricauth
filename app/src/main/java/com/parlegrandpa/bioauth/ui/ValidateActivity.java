package com.parlegrandpa.bioauth.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.parlegrandpa.bioauth.R;
import com.parlegrandpa.bioauth.util.ManageSQLDatabase;
import com.parlegrandpa.bioauth.util.Validation;

public class ValidateActivity extends AppCompatActivity {

    private EditText txt_account_number;
    private TextView error_message;
    private String accountId;
    private ManageSQLDatabase manageSQLDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_validate);

        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        manageSQLDatabase = new ManageSQLDatabase(this);

        init();
    }

    private void init()
    {
        error_message = (TextView) findViewById(R.id.error_message);
        txt_account_number = (EditText) findViewById(R.id.txt_account_number);
        txt_account_number.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                Validation.isPhoneNumber(txt_account_number, false);
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after){}
            public void onTextChanged(CharSequence s, int start, int before, int count){}
        });

        Button btn_validate = (Button) findViewById(R.id.btn_validate);
        btn_validate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(checkValidation())
                {
                    accountId = txt_account_number.getText().toString();
                    boolean checkAccount = manageSQLDatabase.recordExist(accountId);
                    if(checkAccount)
                    {
                        Intent intent = new Intent(ValidateActivity.this, CheckFingerprintActivity.class);
                        intent.putExtra("accountId", accountId);
                        startActivity(intent);
                    }
                    else
                    {
                        error_message.setText(getString(R.string.string_error_message));
                    }
                }
                else
                {
                    error_message.setText(getString(R.string.string_invalid_account));
                }
			}
        });
    }

    private boolean checkValidation() {
        boolean ret = true;

        if (!Validation.isPhoneNumber(txt_account_number, false)
                || (txt_account_number.getText().length() == 0)
                || (txt_account_number.getText().length() > 12) )
        {
            ret = false;
        }
        return ret;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
