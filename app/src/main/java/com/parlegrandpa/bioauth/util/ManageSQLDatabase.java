package com.parlegrandpa.bioauth.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.SparseArray;

import java.util.HashMap;

/**
 * Created by HAYO on 5/14/2018.
 */

public class ManageSQLDatabase extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "paul_fprecord.db";

    private static final String TABLE_NAME = "main";
    private static final String COLUMN_ID = "_id";
    private static final String COLUMN_NAME = "name";
    private static final String COLUMN_LEFT_FPDATA = "left_data";
    private static final String COLUMN_RIGHT_FPDATA = "right_data";

    public ManageSQLDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        try {
            sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" +
                    COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COLUMN_NAME + " TEXT, " +
                    COLUMN_LEFT_FPDATA + " BLOB, " +
                    COLUMN_RIGHT_FPDATA + " BLOB)"
            );
        }
        catch (SQLiteException ignored)
        {
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
    }

    public boolean insertRecord ( String accountNumber, byte[] leftFinger, byte[] rightFinger)
    {
        SQLiteDatabase db = getWritableDatabase();
        try
        {
            ContentValues contentValues = new ContentValues();
            contentValues.put(COLUMN_NAME, accountNumber);
            contentValues.put(COLUMN_LEFT_FPDATA, leftFinger);
            contentValues.put(COLUMN_RIGHT_FPDATA, rightFinger);

            db.insert(TABLE_NAME, null, contentValues);

            return true;
        }
        catch (SQLiteException e)
        {
            return false;
        }
    }

    public SparseArray<byte[]> fetchRecord (String accountID)
    {
        SparseArray<byte[]> arrayStoredFingerData = new SparseArray<byte[]>();
        String getData = "SELECT " +
                COLUMN_LEFT_FPDATA + " , " +
                COLUMN_RIGHT_FPDATA + " , " +
                COLUMN_NAME + " " +
                "FROM " + TABLE_NAME  + " WHERE " +
                COLUMN_NAME + " = '" + accountID + "'";

        SQLiteDatabase db = getWritableDatabase();

        Cursor mcursor = db.rawQuery(getData, null);
        mcursor.moveToFirst();
        arrayStoredFingerData.put(0, mcursor.getBlob(0));
        arrayStoredFingerData.put(1, mcursor.getBlob(1));
        mcursor.close();
        return arrayStoredFingerData;
    }

    public boolean recordExist (String account)
    {
        SQLiteDatabase db = getWritableDatabase();
        String count = "SELECT * FROM " + TABLE_NAME + " WHERE " +
                COLUMN_NAME + " = '" + account + "'";
        Cursor mcursor = db.rawQuery(count, null);
        int icount = mcursor.getCount();
        if(icount > 0)
        {
            return true;
        }
        mcursor.close();
        return false;
    }
}
